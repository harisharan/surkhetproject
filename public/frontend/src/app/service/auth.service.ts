import {Injectable} from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';
import {BehaviorSubject} from 'rxjs';
import {Router} from "@angular/router";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private action = "auth/logout";

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
  }

  private messages = new BehaviorSubject('');

  messageSource = this.messages.asObservable();

  setMessage(message: string) {
    this.messages.next(message);
  }


  /**
   * Check if user is logged in or not
   *
   * @returns {boolean}
   */
  isUserLoggedIn(): boolean {
    let accessToken = localStorage.getItem("access_token");
    // alert(accessToken);
    if (accessToken) {
      const jwtHelper = new JwtHelperService();
      const isExpired = jwtHelper.isTokenExpired(accessToken);

      if (!isExpired) {
        return true;
      }
    }
    return false;
  }

  wrongAuthData(data) {
    if (data) {
      this.setMessage(data);
    } else {
      this.setMessage("Invalid session");
    }

    this.sessionDestroy();
  }

  sessionDestroy() {
    let accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      localStorage.removeItem('access_token');
      localStorage.removeItem('refresh_token');
      localStorage.removeItem('msisdn');
    }
    this.router.navigateByUrl('/login');

  }
}
