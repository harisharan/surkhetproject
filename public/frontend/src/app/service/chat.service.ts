import {Injectable} from '@angular/core';
import * as io from 'socket.io-client';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  private url = 'http://110.44.113.104:1300';
  // private url = 'http://localhost:1300';
  private socket;

  constructor(private http: HttpClient) {
    this.socket = io(this.url);
  }

  public sendMessage(message) {
    // this.socket.emit('new-message', JSON.stringify(message));
    this.socket.emit('new-message', message);
  }

  public getMessages = () => {
    return Observable.create((observer) => {
      this.socket.on('new-message', (message) => {
        console.log(typeof message);
        console.log(message);
        observer.next(message);
      });
    });
  };

}
