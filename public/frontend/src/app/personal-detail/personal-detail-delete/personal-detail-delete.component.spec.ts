import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalDetailDeleteComponent } from './personal-detail-delete.component';

describe('PersonalDetailDeleteComponent', () => {
  let component: PersonalDetailDeleteComponent;
  let fixture: ComponentFixture<PersonalDetailDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonalDetailDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalDetailDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
