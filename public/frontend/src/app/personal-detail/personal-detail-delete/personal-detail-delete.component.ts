import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { AlertMessageService } from "../../service/alert-message.service";
import { takeUntil } from "rxjs/operators";
import { componentDestroyed } from "../../core/takeUntil-function";
import { PersonalDetailService } from "../service/personal-detail.service";

@Component({
  selector: 'personal-detail-delete',
  templateUrl: './personal-detail-delete.component.html',
  styleUrls: ['./personal-detail-delete.component.css']
})
export class PersonalDetailDeleteComponent implements OnInit {
  showConfirmBox: boolean = true;
  private ID: number;
  private DeleteTitle: string = "व्यक्तिको विवरण मेटाउनुहोस्";
  private DeleteWarning: string = "के तपाईँ निश्चित हुनुहुन्छ। के तपाईं व्यक्तिको डाटा मेटाउन चाहनुहुन्छ?";
  private mainPage: string = "/main/personal-detail/";

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: PersonalDetailService,
    private alertMessageService: AlertMessageService) {
    this.route.params.pipe(takeUntil(componentDestroyed(this))).subscribe(params => this.ID = parseInt(params['id']));
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
  }

  confirm($event) {
    if ($event) {
      this.service.remove(this.ID).pipe(takeUntil(componentDestroyed(this))).subscribe(
        data => {
          // if (data['code'] === 0) {
          this.service.setNotify(true);
          this.alertMessageService.show({
            message: " व्यक्तिगत विवरण सफलतापूर्वक हटाइयो",
            alertType: "success"
          });
          this.redirectMainPage();
          // }
        },
        err => {
          this.alertMessageService.show({
            message: err.error.result,
            alertType: "error"
          });
        })
    }
  }

  cancel($event) {
    if ($event) {
      this.showConfirmBox = false;
      this.redirectMainPage();
    }
  }

  redirectMainPage(): void {
    this.router.navigate([this.mainPage]);
  }

}
