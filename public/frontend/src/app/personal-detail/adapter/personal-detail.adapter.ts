import {Personaldetail} from "../model/personal-detail.model";
import {Adapter} from "../../core/adapter";
import {Injectable} from "@angular/core";

@Injectable()
export class PersonaldetailAdapter implements Adapter<Personaldetail> {
  adapt(item: any): Personaldetail {
    console.log(item);
    return new Personaldetail(
      item.id,
      item.person_image,
      item.person_name,
      item.age,
      item.blood_group,
      item.education,
      item.occupation,
      item.religion,
      item.father_name,
      item.mother_name,
      item.grand_father_name,
      item.spouse_name,
      item.time_interval_at_foreign,
      item.foreign_occupation,
      item.created_at,
      item.updated_at,
      item.houses,
      item.member_details,
      item.total,
    );
  }
}

