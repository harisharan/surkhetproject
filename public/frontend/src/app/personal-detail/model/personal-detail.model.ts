export class Personaldetail {
  constructor(
    public id: string,
    public person_image: string,
    public person_name: string,
    public age: Number,
    public blood_group: string,
    public education: string,
    public occupation: string,
    public religion: string,
    public father_name: string,
    public mother_name: string,
    public grand_father_name: string,
    public spouse_name: string,
    public time_interval_at_foreign: string,
    public foreign_occupation: string,
    public created_at: Date,
    public updated_at: Date,
    public houses: Array<any>,
    public members: Array<any>,
    public total_rows: number
  ) {

  }

}
