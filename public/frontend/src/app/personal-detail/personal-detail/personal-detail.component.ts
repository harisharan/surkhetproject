import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { GetWindowHeightService } from "../../service/get-window-height.service";
import { takeUntil } from "rxjs/operators";
import { componentDestroyed } from "../../core/takeUntil-function";
import { PersonalDetailService } from "../service/personal-detail.service";

@Component({
  selector: 'personal-detail',
  templateUrl: './personal-detail.component.html',
  styleUrls: ['./personal-detail.component.css']
})
export class PersonalDetailComponent implements OnInit {


  searchForm: FormGroup;
  innerWidth;
  laptopSize: boolean = false;
  p: number = 1;
  collection = [];
  sortingParams;
  showExpandBtn = true;
  perPageItem = 20;
  totalRows;
  @ViewChild('employeeTable') elementView: ElementRef;
  BASEIPURL: string;
  loader: boolean = false;
  hoverCheckBox: boolean = false;
  displayID;
  displayData;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private service: PersonalDetailService,
    private getWindowHeightService: GetWindowHeightService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.initSearchForm();
    this.initSortingParams();
    this.adjustWidth();
    setTimeout(() => {
      this.initTable();
    }, 20);
    // console.log(this.route.snapshot.data.data_resolve);
    this.BASEIPURL = this.service.getBaseIPURL();
    this.service.getNotify().subscribe(data => {
      this.getData();
    })
  }

  initSortingParams(): void {
    this.sortingParams = {
      "order_direction": "ASC"
    };
  }

  initTable() {
    this.initSortingParams();
    let tableHeaderHeight = 42;
    let tableRowsHeight = 60;
    this.getWindowHeightService.getData(this.elementView, tableHeaderHeight, tableRowsHeight, "empTable");

    this.getWindowHeightService.windowSizeChanged.pipe(takeUntil(componentDestroyed(this))).subscribe(
      value => {
        this.perPageItem = 50;
        this.getData();
      }
    );
  }


  getData() {
    let params = {
      "limit": this.perPageItem,
      "page_number": this.p,
      "order_direction": this.sortingParams.order_direction
    };
    let filterParams = this.getFilterParams();
    if (filterParams != null) {
      params['filters'] = filterParams;
    }
    this.loader = true;
    this.getList(params);
  }

  getList(params) {
    this.service.get(params).pipe(takeUntil(componentDestroyed(this))).subscribe(data => {
      this.totalRows = typeof data['error'] === 'undefined' ? data[0].total_rows : 0;
      this.collection = data;
      this.loader = false;
    }, error => console.log(error));

  }

  expandTable() {
    this.showExpandBtn = false;
  }

  pageChanged(pageNumber): void {
    this.p = pageNumber;
    this.getData();
  }

  ngOnDestroy(): void {
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.adjustWidth();
  }

  adjustWidth(): void {
    this.innerWidth = window.innerWidth;
    if (this.innerWidth <= 1100) {
      this.laptopSize = true;
    } else {
      this.laptopSize = false;
    }
  }

  closeCard() {
    this.displayID = null;
  }

  addNewEmployee() {
    this.router.navigate(['main/personal-detail/create']);
  }

  edit(id) {
    this.router.navigate(['main/personal-detail/edit/' + id]);
  }

  view(id) {
    this.service.getByID(id).pipe(takeUntil(componentDestroyed(this))).subscribe(data => {
      this.displayID = id;
      this.displayData = data;
    }, error => console.log(error));
    // console.log("send message to employee");
  }

  delete(id) {
    this.router.navigate(["main/personal-detail/delete/" + id]);
  }

  private errorImage = false;

  errorImgHandler(event) {
    event.target.src = "assets/icons/blue_user.svg";
    event.target.classList.add("img-error");
  }

  initSearchForm() {
    this.searchForm = this.formBuilder.group({
      'person_name': [''],
      'age': ['', [Validators.pattern('^[0-9]*$')]],
      'blood_group': [''],
      'occupation': [''],
      'house_number': [''],
      'member_name': [''],
    });

  }

  search() {
    if (this.searchForm.valid) {
      this.p = 1;
      this.getData();
    }
  }

  getFilterParams() {
    if (this.searchForm.valid) {
      let filterParams = {};
      if (this.searchForm.value.person_name != "" && this.searchForm.value.person_name != null) {
        filterParams["person_name"] = this.searchForm.value.person_name;
      }
      if (this.searchForm.value.age != "" && this.searchForm.value.age != null) {
        filterParams["age"] = this.searchForm.value.age;
      }

      if (this.searchForm.value.blood_group != "" && this.searchForm.value.blood_group != null) {
        filterParams["blood_group"] = this.searchForm.value.blood_group;
      }

      if (this.searchForm.value.occupation != "" && this.searchForm.value.occupation != null) {
        filterParams["occupation"] = this.searchForm.value.occupation;
      }
      if (this.searchForm.value.house_number != "" && this.searchForm.value.house_number != null) {
        filterParams["house_number"] = this.searchForm.value.house_number;
      }
      if (this.searchForm.value.member_name != "" && this.searchForm.value.member_name != null) {
        filterParams["member_name"] = this.searchForm.value.member_name;
      }
      return filterParams;
    }
    return null;
  }

  sortItems($event): any {
    this.sortingParams = {
      "order_direction": $event.order_direction
    };
    this.getData();
  }


  setCheckBoxHover($event) {
    this.hoverCheckBox = $event;
  }




  exportTableToExcel() {
    let htmltable = document.getElementById('printReport');
    let html = htmltable.outerHTML;
    window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
  }

  exportTableToPdf() {
    let printContents, popupWin;
    printContents = '';
    printContents = document.getElementById('printReport').outerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Report</title>
          <style>
          @font-face {
            font-family: "nepaliNumber";
            src: url("../../../assets/css/Shangrila-Numeric.TTF");
            font-weight: normal;
            font-style: normal;
          }
          
          @font-face {
            font-family: "nepaliNormal";
            src: url("../../../assets/css/Kantipur.TTF");
            font-weight: normal;
            font-style: normal;
          }
          
          .nepali-number {
            font-family: "nepaliNumber" !important;
            font-size: 18px !important;
          }
          .nepali-text {
            font-family: "nepaliNormal" !important;
            font-size: 22px !important;
          }
          .table {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
            border: 1px solid black;
            border-collapse: collapse;
          }
          td, th {
            border: 1px solid black;
          }
          .tbl-employee-list[_ngcontent-pjy-c4] {
            margin-bottom: 10px;
            float: left;
            width: 100%;
          }
          ul {
            list-style-type:none;
          }
          thead {
            display: table-header-group;
            vertical-align: middle;
            border-color: inherit;
          }
          .text-align-center[_ngcontent-pjy-c4] {
            text-align: center !important;
        }
          </style>
        </head>
      <body onload="window.print();">
        <h2 style="text-align:center;">ब्यक्तिगत विवरणको रिपोर्ट</h2>
        <hr style="color:black;">
        <div>
          ${printContents}
        </div>
      </body>
      </html>`
    );
    popupWin.document.close();
  }
}
