import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AppConfigService} from '../../service/AppConfig.service';
import {PersonaldetailAdapter} from "../adapter/personal-detail.adapter";

@Injectable()
export class PersonalDetailService {
  private URL: string;
  private queryString: string = '?action=';
  private getMethod = 'get_employees';
  private getFreeEmployeeMethod = 'get_free_employee';
  private getGroupEmployee = 'get_employee_from_group';
  private insertMethod = 'insert_employee';
  private deleteMethod = 'delete_employee';
  private updateMethod = 'update_employee';
  private getEmployeeListMethod = 'get_employee_user_list';

  constructor(
    private appConfigService: AppConfigService,
    private http: HttpClient,
    private adapter: PersonaldetailAdapter,
  ) {
  }


  getBaseIPURL(): string {
    return this.appConfigService.config['base_ip'];
  }

  getURL(action: string): string {
    this.URL = this.appConfigService.config['base_url'] + this.queryString;
    return this.URL + action;
  }

  get(params): Observable<any> {
    console.log(params);
    let sort = "?" + "sort=" + params.order_direction;
    let page = "&&" + "page=" + params.page_number;
    let limit = "&&" + "limit=" + params.limit;
    let person_name = "";
    let age = "";
    let blood_group = "";
    let occupation = "";
    let house_number = "";
    let member_name = "";
    console.log(params.filters);
    if (typeof params.filters !== "undefined") {
      person_name = params.filters.person_name === "" || typeof params.filters.person_name === "undefined" ? "" : "&&" + "person_name=" + params.filters.person_name;
      age = params.filters.age === "" || typeof params.filters.age === "undefined" ? "" : "&&" + "age=" + params.filters.age;
      blood_group = params.filters.blood_group === "" || typeof params.filters.blood_group === "undefined" ? "" : "&&" + "blood_group=" + params.filters.blood_group;
      occupation = params.filters.occupation === "" || typeof params.filters.occupation === "undefined" ? "" : "&&" + "occupation=" + params.filters.occupation;
      house_number = params.filters.house_number === "" || typeof params.filters.house_number === "undefined" ? "" : "&&" + "house_number=" + params.filters.house_number;
      member_name = params.filters.member_name === "" || typeof params.filters.member_name === "undefined" ? "" : "&&" + "member_name=" + params.filters.member_name;
    }
    return this.http.get(this.appConfigService.config['base_url2'] + 'personal_detail' + sort + limit + page + person_name + age + blood_group + occupation + house_number + member_name).pipe(
      map((data: any[]) => {
          console.log(data);
          // if (data['error']['success_code'] === 0) {
          //     return data;
          // }
          console.log(typeof data['error'] === 'undefined');
          if (typeof data['error'] === 'undefined') {
            return data['data']['data'].map((item: any) => {
              item.total = data['data']['total'];
              return this.adapter.adapt(item)
            });
          } else {
            return data;
          }

        }
      )
    );
    // // let method = data.type ? (data.type === 'group_employee' ? this.getGroupEmployee : (data.type === 'get_employee_user_list' ? this.getEmployeeListMethod : this.getFreeEmployeeMethod)) : this.getMethod;
    // return this.http.get(this.getURL(this.appConfigService.config['base_url2'] + 'personal_detail')).pipe(
    //   map((data: any[]) => {
    //       if (typeof data['result'] === 'number') {
    //         return data['result'];
    //       } else if (data['code'] === 0) {
    //         return data['result'].map((data: any[]) => this.employeeTblAdapter.adapt(data));
    //       }
    //     }
    //   )
    // );
  }


  save(data): Observable<any> {
    console.log(data);
    return this.http.post(this.appConfigService.config['base_url2'] + 'personal_detail', data).pipe();
  }

  updateEmployees(data): Observable<any> {
    return this.http.post(this.getURL(this.updateMethod), data).pipe();
  }

  getByID(ID): Observable<any> {
    return this.http.get(this.appConfigService.config['base_url2'] + 'personal_detail/' + ID).pipe(
      map((data: any[]) => {
        console.log(data);
        // if (data['error']['success_code'] === 0) {
        //     return data;
        // }
        console.log(typeof data['error'] === 'undefined');
        if (typeof data['error'] === 'undefined') {
          // return data['data'].map((item: any) => {
          // item.total = data['data']['total'];
          return this.adapter.adapt(data['data'])
          // });
        } else {
          return data;
        }

      }));
    // map((data: any[]) => {
    //   return this.adapter.adapt(data['result'][0]);
    // })
    // );
  }

  remove(ID): Observable<any> {

    return this.http.delete(this.appConfigService.config['base_url2'] + 'personal_detail/' + ID);
  }

  private notify = new BehaviorSubject([]);

  getNotify(): Observable<any> {
    return this.notify.asObservable();
  }

  setNotify(data): void {
    this.notify.next(data);
  }
}
