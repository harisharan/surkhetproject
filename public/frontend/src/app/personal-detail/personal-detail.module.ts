import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PersonalDetailRoutingModule} from './personal-detail-routing.module';
import {PersonalDetailComponent} from './personal-detail/personal-detail.component';
import {MainModule} from "../main/main.module";
import {SearchIconFormModule} from "@shared/search-icon-form/search-icon-form.module";
import {SelectInputModule} from "@shared/select-input/select-input.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ImageUploadModule} from "@shared/image-upload/image-upload.module";
import {TextMaskModule} from "angular2-text-mask";
import {SharedButtonModule} from "@shared/shared-button/shared-button.module";
import {DbEmpActionModule} from "@shared/db-emp-action/db-emp-action.module";
import {NgxPaginationModule} from "ngx-pagination";
import {TranslateModule} from "@ngx-translate/core";
import {InvalidMessageModule} from "@shared/form-message/invalid-message.module";
import {DeleteDialogModule} from "@shared/delete-dialog/delete-dialog.module";
import {CheckboxModule} from "@shared/checkbox/checkbox.module";
import {SortingModule} from "@shared/directive-sorting/sorting.module";
import {PersonaldetailAdapter} from "./adapter/personal-detail.adapter";
import {PersonalDetailService} from "./service/personal-detail.service";
import {ErrorInterceptorProvider} from "../core/http-interceptor.service";
import {GetWindowHeightService} from "../service/get-window-height.service";
import {PersonalDetailCreateComponent} from './personal-detail-create/personal-detail-create.component';
import {PersonalDetailDeleteComponent} from './personal-detail-delete/personal-detail-delete.component';
import {InfoCardModule} from "@shared/info-card/info-card.module";
import {HttpConfigInterceptorProvider} from "../core/httpconfig.interceptor";

@NgModule({
  declarations: [PersonalDetailComponent, PersonalDetailCreateComponent, PersonalDetailDeleteComponent],
  imports: [
    CommonModule,
    PersonalDetailRoutingModule,
    MainModule,
    SearchIconFormModule,
    SelectInputModule,
    FormsModule,
    ReactiveFormsModule,
    ImageUploadModule,
    TextMaskModule,
    SharedButtonModule,
    DbEmpActionModule,
    NgxPaginationModule,
    TranslateModule,
    InvalidMessageModule,
    DeleteDialogModule,
    CheckboxModule,
    SortingModule, InfoCardModule
  ],
  providers: [PersonalDetailService, PersonaldetailAdapter, HttpConfigInterceptorProvider,
    {provide: 'windowObject', useValue: window},
    GetWindowHeightService]
})
export class PersonalDetailModule {
}
