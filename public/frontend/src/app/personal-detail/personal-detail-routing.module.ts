import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {PersonalDetailComponent} from "./personal-detail/personal-detail.component";
import {PersonalDetailCreateComponent} from './personal-detail-create/personal-detail-create.component';
import {PersonalDetailDeleteComponent} from "./personal-detail-delete/personal-detail-delete.component";

const routes: Routes = [
  {
    path: '',
    component: PersonalDetailComponent,
    children: [
      {path: 'delete/:id', component: PersonalDetailDeleteComponent}
    ]
  },
  {
    path: 'create',
    component: PersonalDetailCreateComponent,
  }, {
    path: 'edit/:id',
    component: PersonalDetailCreateComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonalDetailRoutingModule {
}
