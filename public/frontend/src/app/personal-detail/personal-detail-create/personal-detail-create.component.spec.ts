import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalDetailCreateComponent } from './personal-detail-create.component';

describe('PersonalDetailCreateComponent', () => {
  let component: PersonalDetailCreateComponent;
  let fixture: ComponentFixture<PersonalDetailCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonalDetailCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalDetailCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
