import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AlertMessageService } from "../../service/alert-message.service";
import { takeUntil } from "rxjs/operators";
import { componentDestroyed } from "../../core/takeUntil-function";
import { PersonalDetailService } from "../service/personal-detail.service";

@Component({
  selector: 'personal-detail-create',
  templateUrl: './personal-detail-create.component.html',
  styleUrls: ['./personal-detail-create.component.css']
})
export class PersonalDetailCreateComponent implements OnInit {
  public mask = ['0', '6', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]
  newForm: FormGroup;
  infoText: string = "{time}";
  role: number = 2;
  isSubmited: boolean = false;
  mainPage = '/main/personal-detail';
  fileData: File;
  ID: number;
  PageTitle: string = "Add new detail";
  BtnTitle: string = 'Next';
  avatarUrl: string;
  BASEIPURL: string;
  loader: boolean = false;
  firstStep: boolean = true;
  secondStep: boolean = false;
  thirdStep: boolean = false;
  btnDisabled: boolean = false;

  occupationList =
    [
      { id: "1", name: "कृषि" },
      { id: "2", name: "सरकारी" },
      { id: "2", name: "गैर-सरकारी" },
      { id: "3", name: "व्यापार" },
    ];
  foreignOccupationList =
    [
      { id: "1", name: "मलेशिया" },
      { id: "2", name: "कतार" },
      { id: "3", name: "दुबई" },
      { id: "4", name: "अमेरिका" },
      { id: "5", name: "अष्ट्रेलिया" },
      { id: "6", name: "अन्य" },
    ];
  houseTypeList =
    [
      { id: "1", name: "सिमेन्टेड" },
      { id: "2", name: "सिमेन्टे नलगाको" }
    ];
  religionList =
    [
      { id: "1", name: "हिन्दू" },
      { id: "2", name: "बौद्ध" },
      { id: "3", name: "मुस्लिम" },
      { id: "4", name: "क्रिश्चियन" }
    ];
  bloodGroupList = [
    { id: "1", name: "A+" },
    { id: "2", name: "A-" },
    { id: "3", name: "B+" },
    { id: "4", name: "B-" },
    { id: "5", name: "AB+" },
    { id: "6", name: "AB-" },
    { id: "7", name: "O+" },
    { id: "8", name: "O-" }
  ];

  tolList =
    [
      { id: "1", name: "भैरव टोल बिकास संस्था।" },
      { id: "2", name: "समाबेशी टोल बिकास संस्था" },
      { id: "3", name: "मिलन टोल बिकास संस्था" },
      { id: "4", name: "मंगल टोल बिकास संस्था" },
      { id: "5", name: "एकता टोल बिकास संस्था" },
      { id: "6", name: "नयाबजार टोल बिकास संस्था" },
      { id: "7", name: "कृष्ण चोक टोल बिकास संस्था" },
      { id: "8", name: "गौरी टोल बिकास संस्था" },
      { id: "9", name: "उज्वोल टोल बिकास संस्था" },
      { id: "10", name: "चन्द्र चोक टोल बिकास संस्था" },
      { id: "11", name: "हाम्रो टोल बिकास संस्था" },
      { id: "12", name: "सूर्य मुखी टोल बिकास संस्था" },
      { id: "13", name: "सीतल टोल बिकास संस्था" },
      { id: "14", name: "मित्रनगर टोल बिकास संस्था" },
      { id: "15", name: "सहिद स्मृति टोल बिकास संस्था" },
      { id: "16", name: "गतिसिल टोल बिकास संस्था" },
      { id: "17", name: "संझौता टोल बिकास संस्था" },
      { id: "18", name: "कालागाउँ टोल बिकास संस्था" },
      { id: "19", name: "नवज्योति टोल बिकास संस्था" },
      { id: "20", name: "पीपल चौतारी टोल बिकस संस्था" },
    ];

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private service: PersonalDetailService,
    private alertService: AlertMessageService,
    private render: Renderer2,
    private route: ActivatedRoute) {
    this.route.params.pipe(takeUntil(componentDestroyed(this))).subscribe(params => this.ID = parseInt(params['id']));
  }

  ngOnInit() {
    this.initForm();
    // this.addNewHouse();
    // this.addNewMember();
    this.editInfo();
    this.BASEIPURL = this.service.getBaseIPURL();
  }

  editInfo() {
    if (this.ID) {
      this.PageTitle = "Edit detail";
      // this.BtnTitle = "Update";
      this.service.getByID(this.ID).pipe(takeUntil(componentDestroyed(this))).subscribe(data => {
        console.log(data);
        this.newForm.patchValue({
          id: data.id,
          // person_image: data.person_image,
          person_name: data.person_name,
          age: data.age,
          blood_group: data.blood_group,
          education: data.education,
          occupation: data.occupation,
          religion: data.religion,
          father_name: data.father_name,
          mother_name: data.mother_name,
          grand_father_name: data.grand_father_name,
          spouse_name: data.spouse_name,
          time_interval_at_foreign: data.time_interval_at_foreign,
          foreign_occupation: data.foreign_occupation,
        });
        this.avatarUrl = (data.person_image != "" && data.person_image != null) ? this.BASEIPURL + data.person_image : "";
        if (this.avatarUrl) {
          this.removeValidationFromImg();
        } else {
          this.setValidationOnImg();
        }
        let control = <FormArray>this.newForm.controls.houses;
        data['houses'].forEach(x => {
          control.push(this.formBuilder.group({
            tol_number: x.tol_number,
            house_type: x.house_type,
            house_area: x.house_area,
          }));
        });
        let control1 = <FormArray>this.newForm.controls.members;
        data['members'].forEach(x => {
          control1.push(this.formBuilder.group({
            member_name: x.member_name,
            member_age: x.member_age,
            member_blood_group: x.member_blood_group,
            member_education: x.member_education,
            member_occupation: x.member_occupation,
            member_religion: x.member_religion,
            member_relation: x.member_relation,
            member_father_name: x.member_father_name,
            member_mother_name: x.member_mother_name,
            member_grand_father_name: x.member_grand_father_name,
            member_spouse_name: x.member_spouse_name
          }));
        });
      });
    }
  }


  initForm() {
    this.newForm = this.formBuilder.group({
      id: [""],
      person_image: ["", [Validators.required]],
      person_name: ["", [Validators.required]],
      age: ["", [Validators.required,
      Validators.pattern('^[0-9]*$')
      ]],
      blood_group: ["", [Validators.required]],
      education: ["", [Validators.required]],
      occupation: ["", [Validators.required]],
      religion: ["", [Validators.required]],
      father_name: ["", [Validators.required]],
      mother_name: ["", [Validators.required]],
      grand_father_name: ["", [Validators.required]],
      spouse_name: [""],
      time_interval_at_foreign: ["", [
        Validators.pattern('^[0-9]*$')
      ]],
      foreign_occupation: [""],
      houses: this.formBuilder.array([]),
      members: this.formBuilder.array([])
    })
  }


  setValidationOnImg() {
    const imageControl = this.newForm.get('person_image');
    imageControl.setValidators([Validators.required]);
    imageControl.updateValueAndValidity();
  }

  removeValidationFromImg() {
    const imageControl = this.newForm.get('person_image');
    imageControl.setValue("");
    imageControl.clearValidators();
    imageControl.updateValueAndValidity();

  }

  btnBackClicked() {
    if (this.firstStep === true) {
      this.router.navigate(['main/personal-detail']);
      console.log("back btn clicked");
    } else if (this.secondStep === true) {
      this.BtnTitle = "Next";
      this.secondStep = false;
      this.firstStep = true;
      this.thirdStep = false;
    } else if (this.thirdStep === true) {
      this.BtnTitle = "Next";
      this.secondStep = true;
      this.firstStep = false;
      this.thirdStep = false;
    }

  }

  changeAvatar($event) {
    this.fileData = <File>$event.file;
    console.log(this.fileData);
  }

  setValidators(controlName): void {
    controlName.setValidators([Validators.required]);
    controlName.updateValueAndValidity();
  }

  storeData() {
    this.isSubmited = true;
    if (this.firstStep === true) {
      if (this.newForm.valid) {
        this.BtnTitle = "Next";
        this.secondStep = true;
        this.firstStep = false;
        this.thirdStep = false;
        this.addNewHouse();
      }
    } else if (this.secondStep === true) {
      if (this.newForm.valid) {
        this.addNewMember();
        this.BtnTitle = this.ID ? "Update" : "Save";
        this.secondStep = false;
        this.firstStep = false;
        this.thirdStep = true;
      }
    } else {
      if (this.newForm.valid) {
        this.btnDisabled = true;
        this.loader = true;
        let data = this.newForm.value;
        const uploadData = new FormData();
        if ((this.newForm.get('person_image').value) !== "") {
          uploadData.append('person_image', this.fileData);
        }
        uploadData.append('person_name', this.newForm.get('person_name').value);
        uploadData.append('age', this.newForm.get('age').value);
        uploadData.append('blood_group', this.newForm.get('blood_group').value);
        uploadData.append('education', this.newForm.get('education').value);
        uploadData.append('occupation', this.newForm.get('occupation').value);
        uploadData.append('religion', this.newForm.get('religion').value);
        uploadData.append('father_name', this.newForm.get('father_name').value);
        uploadData.append('mother_name', this.newForm.get('mother_name').value);
        uploadData.append('grand_father_name', this.newForm.get('grand_father_name').value);
        uploadData.append('spouse_name', this.newForm.get('spouse_name').value);
        uploadData.append('foreign_occupation', this.newForm.get('foreign_occupation').value);
        uploadData.append('time_interval_at_foreign', this.newForm.get('time_interval_at_foreign').value);
        uploadData.append('houses', JSON.stringify(this.newForm.get('houses').value));
        uploadData.append('members', JSON.stringify(this.newForm.get('members').value));

        this.service.save(uploadData).subscribe(data => {
          console.log(data);
          if (data['success'] === true) {
            this.success("सफलतापूर्वक व्यक्तिगत विवरण सिर्जना गरियो");
          } else {
            this.error("कुनै डाटा सिर्जना गरिएको छैन");
          }
        }, err => {
          this.error(err.error.messsage);
        })
        console.log(uploadData);
      }
    }
  }




  ngOnDestroy(): void {
  }

  addNewHouse() {
    if (!this.ID) {
      let control = <FormArray>this.newForm.controls.houses;
      control.push(
        this.formBuilder.group({
          tol_number: ['', [Validators.required]],
          house_area: ['', [Validators.required]],
          house_type: ['', [Validators.required]],
        })
      )
    }
  }

  addNewMember() {
    if (!this.ID) {
      let control = <FormArray>this.newForm.controls.members;
      control.push(
        this.formBuilder.group({
          member_name: ['', [Validators.required]],
          member_age: ['', [Validators.required,
          Validators.pattern('^[0-9]*$')
          ]],
          member_blood_group: ['', [Validators.required]],
          member_education: ['', [Validators.required]],
          member_occupation: ['', [Validators.required]],
          member_religion: ['', [Validators.required]],
          member_relation: ['', [Validators.required]],
          member_father_name: ['', [Validators.required]],
          member_mother_name: ['', [Validators.required]],
          member_grand_father_name: ['', [Validators.required]],
          member_spouse_name: [''],
        })
      )
    }
  }

  deleteHouse(index) {
    let control = <FormArray>this.newForm.controls.houses;
    control.removeAt(index);
  }

  deleteMember(index) {
    let control = <FormArray>this.newForm.controls.members;
    control.removeAt(index);
  }

  redirectMainPage() {
    this.router.navigate([this.mainPage]);
  }

  success(message): void {
    this.loader = false;
    this.btnDisabled = false;
    this.alertService.show({
      message: message,
      alertType: "success"
    });
    this.redirectMainPage();
  }

  error(err): void {
    console.log(err.error.result);
    this.alertService.show({
      message: err.error.result,
      alertType: "error"
    });
  }

  get f() {
    return this.newForm.controls;
  }

}
