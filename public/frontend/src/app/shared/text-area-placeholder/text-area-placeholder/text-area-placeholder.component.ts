import {Component, OnInit, ElementRef, ViewChild, SimpleChanges, Output, EventEmitter, Renderer2, Input, OnChanges} from '@angular/core';

@Component({
  selector: 'text-area-placeholder',
  templateUrl: './text-area-placeholder.component.html',
  styleUrls: ['./text-area-placeholder.component.css']
})
export class TextAreaPlaceholderComponent implements OnInit, OnChanges {
  textArea;
  @ViewChild('textArea') textAreaRef: ElementRef;
  @Input() readonly;
  @Output() textAreaData = new EventEmitter<any>();
  @Output() childEvent = new EventEmitter<any>();
  @Output() caretPos = new EventEmitter<any>();

  @Input() textStr = '';
  isCusPlaceholder = true;

  constructor(
    private renderer: Renderer2
  ) {
  }

  ngOnInit() {
    this.textArea = this.renderer.selectRootElement(this.textAreaRef);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (typeof changes['textStr'] !== 'undefined') {
      if (changes['textStr'].currentValue !== '') {
        this.isCusPlaceholder = false;
        this.onChange('');
      }
    }
  }

  onClick(e) {
    if (!this.readonly) {
      this.textArea.nativeElement.focus();
    }
  }

  onChange(e) {
    if (typeof this.textStr !== 'undefined') {
      if (this.textStr.length > 0) {
        this.isCusPlaceholder = false;
      } else {
        this.isCusPlaceholder = true;
      }
      this.onClick('');
      this.textAreaData.emit(this.textStr);
    }
  }

  getCaretPos(oField) {
    if (oField.selectionStart || oField.selectionStart === '0') {
      console.log(oField.selectionStart);
      this.caretPos.emit(oField.selectionStart);
    }
  }

}
