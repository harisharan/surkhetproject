import { Component } from '@angular/core';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';
import { componentDestroyed } from './core/takeUntil-function';
import { AuthenticationService } from './service/authentication.service';
import { ActivatedRoute, Router,Event,
    NavigationCancel,
    NavigationEnd,
    NavigationError,
    NavigationStart,
     } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Surkhet Project';
  languages = [
    { code: 'En', label: 'English' },
    { code: 'es', label: 'Español' },
    { code: 'fr', label: 'Français' },
    { code: 'Ru', label: 'Russian' }
];
isShowLoginPage: boolean = false;

// for loading indicator
  loading = false;


constructor(
    //@Inject(LOCALE_ID) private  localeId: string ,
    private _translate: TranslateService,
    private authenticationService: AuthenticationService,
    private route: ActivatedRoute,
    private router: Router
) {
    //for authentication
    // if (this.authenticationService.access_tokenValue) {
    //     console.log(this.authenticationService.access_tokenValue);
    //     this.router.navigate(['/main']);
    // }
    // else{
    //     this.isShowLoginPage= true;
    // }
    //for loading indicator
    this.router.events.subscribe((event: Event) => {
        switch (true) {
          case event instanceof NavigationStart: {
            this.loading = true;
            break;
          }

          case event instanceof NavigationEnd:
          case event instanceof NavigationCancel:
          case event instanceof NavigationError: {
            this.loading = false;
            break;
          }
          default: {
            break;
          }
        }
      });
}
ngOnInit() {
    // let defaultLanguage = localStorage.getItem('language');
    // console.log(navigator.languages);
    // if (defaultLanguage) {
    //     this._translate.use(defaultLanguage);
    // }
    // else {
    //    localStorage.setItem("language", "En");
    //     this._translate.setDefaultLang("En");
    //     this._translate.use("En");
    // }
    // this._translate.onLangChange.pipe(takeUntil(componentDestroyed(this))).subscribe((event: LangChangeEvent) => {
    //     console.log(event);
    // });
}
ngOnDestroy(): void {
}

}
