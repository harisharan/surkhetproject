import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { takeUntil } from 'rxjs/operators';
import { componentDestroyed } from 'src/app/core/takeUntil-function';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  /**
   * Login form Group
   *
   * @type FormGroup
   */
  loginForm: FormGroup;
  invalidMessages;
  returnUrl: string;
  // resetPasswordMessage: string = "To reset the password-send SMS with the text RESET to the xxxx Staff Tracker Service Number from Your phone number.";
  resetPasswordMessage: string = "कृपया मान्य पासवर्ड प्रयोग गरी साइन इन गर्न प्रयास गर्नुहोस्। अत्यधिक गोप्य डेटा राख्नुहोस् ।";
  errorMessagePhone;
  errorMessagePassword;
  isShowLoginPage: boolean = false;
  loader: boolean = false;

  constructor(
    private fb: FormBuilder,
    private titleService: Title,
    private authenticationService: AuthenticationService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    // if (this.authenticationService.access_tokenValue) {
    //   console.log(this.authenticationService.access_tokenValue);
    //   this.router.navigate(['/main']);
    // } else {
    //   this.isShowLoginPage = true;
    // }
  }

  ngOnInit() {
    this.titleService.setTitle("Surkhet Project");
    this.createForm();
    this.loginForm.valueChanges.subscribe(
      (data) => {
        console.log(data)
      }
    )
    console.log(this.loginForm)
  }

  /**
   * Create Login form
   *
   * @return void
   */
  createForm(): void {
    this.loginForm = this.fb.group({
      email: ["", [Validators.required]],
      password: ["", [Validators.required]]
    });
  }

  get f() {
    return this.loginForm.controls;
  }

  onSubmit() {
    if (this.loginForm.valid) {
      this.loader = true;
      this.authenticationService.login(this.f.email.value, this.f.password.value)
        .pipe(takeUntil(componentDestroyed(this))).subscribe(
          data => {
            this.router.navigate([this.returnUrl]);
          },
          error => {
            let err = error['error'];
            if (err && err['code'] == -8) {
              this.errorMessagePassword = { "incorrect": "The incorrect phone number or password is entered." }
              this.errorMessagePhone = { "incorrect": "The incorrect phone number or password is entered." }
              this.loginForm.controls['email'].setErrors({ 'incorrect': true });
              this.loginForm.controls['password'].setErrors({ 'incorrect': true });
            } else if (err && err['code'] == -9) {
              this.errorMessagePhone = { "incorrect": err['result'] }
              this.loginForm.controls['email'].setErrors({ 'incorrect': true });
            } else if (err && err['code'] == -7) {

            } else {
              if (err && err['result']) {
                this.errorMessagePhone = { "incorrect": err['result'] };
                this.errorMessagePassword = { "incorrect": err['result'] };
              } else {
                this.errorMessagePhone = { "incorrect": error['statusText'] };
                this.errorMessagePassword = { "incorrect": error['statusText'] };
              }

              this.loginForm.controls['email'].setErrors({ 'incorrect': true });
              this.loginForm.controls['password'].setErrors({ 'incorrect': true });
            }
                this.loader = false;
          },
          () => {
            this.loader = false;
          });
    }
  }

  ngOnDestroy(): void {
  }

  ngAfterContentInit() {
    if (this.route.snapshot.queryParams['returnUrl'] == 'login') {
      this.returnUrl = 'main';
    } else {
      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || 'main';
    }

  }


}

//
