import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {UserProfileModel} from "../model/user-profile.model";
import {AppConfigService} from "./../../../service/AppConfig.service";
import {UserProfileAdapter} from "../adapter/user-profile.adapter";


@Injectable({
  providedIn: 'root'
})
export class UserProfileService {
    private URL: string;
    private queryString: string = "?action=";
    private getMethod = "who_i_am";
    
    constructor(
        private appConfigService: AppConfigService,
        private http: HttpClient,
        private userProfileAdapter: UserProfileAdapter,
        ) { 
    }
    
    getBaseIPURL(): string {
        return this.appConfigService.config['base_ip'];
    }
    
    getURL(action: string): string {
        this.URL = this.appConfigService.config['base_url'] + this.queryString;
        return this.URL + action;
    }
    
    getProfileDetail(): Observable<any> {
        let params = [];
        
        return this.http.post(this.getURL(this.getMethod), params).pipe(
          map((data: any[]) => {
            return this.userProfileAdapter.adapt(data['result'][0]);
          })
        );
    }
    
}

