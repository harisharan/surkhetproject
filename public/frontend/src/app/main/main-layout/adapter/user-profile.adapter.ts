import {Adapter} from "./../../../core/adapter";
import {Injectable} from "@angular/core";
import {UserProfileModel} from "../model/user-profile.model";


@Injectable()

export class UserProfileAdapter implements Adapter<UserProfileModel> {

    adapt(data: any): UserProfileModel {
        return new UserProfileModel(
            data.name,
            data.role,
            data.email,
            data.msisdn,
            data.icon,
            data.position,
            data.group,
            data.employee_id
        );
    }
}
