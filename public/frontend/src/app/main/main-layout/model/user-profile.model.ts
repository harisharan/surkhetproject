export class UserProfileModel {
    constructor(
        public name: string,
	public role: string,
        public email: string,
        public msisdn: string,
        public icon: any,
        public position: string,
	public group: string,
        public employee_id: number
        ){}

}
