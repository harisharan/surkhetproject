import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { AuthGuard } from '../auth.guard';
import { ErrorMessageResolver } from '../core/errorMessageResolver';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    component: MainLayoutComponent,
    resolve: { message: ErrorMessageResolver },

    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      {
        path: 'dashboard',
        canActivate: [AuthGuard],
        component: DashboardComponent,
        resolve: { message: ErrorMessageResolver }
      },
      {
        path: 'personal-detail',
        canActivate: [AuthGuard],
        loadChildren: '../personal-detail/personal-detail.module#PersonalDetailModule'
      },
    ]

  }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    ErrorMessageResolver,
  ]
})
export class MainRoutingModule {
}
