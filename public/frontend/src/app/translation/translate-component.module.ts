import { NgModule } from '@angular/core';
import { TranslateComponent } from './translate-component/translate.component';
import { ClosePanelModule } from '@shared/close-panel/close-panel.module';
import {CommonModule} from '@angular/common';

@NgModule({
  declarations: [
    TranslateComponent
  ],
  imports: [
    ClosePanelModule,
    CommonModule
  ],
  exports: [
    TranslateComponent
  ]
})
export class TranslateComponentModule { }
