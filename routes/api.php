<?php
Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});
Route::group(['middleware' => 'jwt'], function () {
    Route::resource('personal_detail', 'PersonalDetailsController');
    Route::resource('house', 'HouseController');
    Route::get('house/checkUnique/{house_number}', 'HouseController@checkUnique');
    Route::resource('member_detail', 'MemberDetailsController');
});
