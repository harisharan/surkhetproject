<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return public_path();
//    return \File::get(public_path() . '\frontend\index.html');
   return view('welcome');
});
Route::get('/images/{image_name}', function () {
    return \File::get(public_path() . '\images\image_name');
});
