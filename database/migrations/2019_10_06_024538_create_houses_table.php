<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateHousesTable.
 */
class CreateHousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('houses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('person_detail_id')->unsigned();
            $table->foreign('person_detail_id')->references('id')->on('personal_details')->onDelete('cascade');
            $table->string('tol_number');
            $table->string('house_number')->unique();
            $table->integer('house_type');
            $table->string('house_area');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('houses');
    }
}
