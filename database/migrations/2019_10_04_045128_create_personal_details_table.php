<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreatePersonalDetailsTable.
 */
class CreatePersonalDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('person_image')->nullable();
            $table->string('person_name');
            $table->integer('age');
            $table->string('blood_group');
            $table->string('education');
            $table->string('occupation');
            $table->string('religion');
            $table->string('father_name');
            $table->string('mother_name');
            $table->string('grand_father_name');
            $table->string('spouse_name')->nullable();
            $table->integer('time_interval_at_foreign')->nullable();
            $table->string('foreign_occupation')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('personal_details');
    }
}
