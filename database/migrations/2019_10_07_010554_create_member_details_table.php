<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateMemberDetailsTable.
 */
class CreateMemberDetailsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('member_details', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('person_detail_id')->unsigned();
            $table->foreign('person_detail_id')->references('id')->on('personal_details')->onDelete('cascade');
            $table->string('member_name');
            $table->integer('member_age');
            $table->string('member_blood_group');
            $table->string('member_education');
            $table->string('member_occupation');
            $table->string('member_religion');
            $table->string('member_relation');
            $table->string('member_father_name');
            $table->string('member_mother_name');
            $table->string('member_grand_father_name');
            $table->string('member_spouse_name')->nullable();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('member_details');
	}
}
