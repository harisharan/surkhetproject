<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class MemberDetails.
 *
 * @package namespace App\Entities;
 */
class MemberDetails extends Model implements Transformable
{
    use TransformableTrait;

    protected $table='member_details';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'person_detail_id',
        'member_name',
        'member_age',
        'member_blood_group',
        'member_education',
        'member_occupation',
        'member_religion',
        'member_relation',
        'member_father_name',
        'member_mother_name',
        'member_grand_father_name',
        'member_spouse_name',
    ];

    public function peronalDetails(){
        return $this->belongsTo('App\Entities\PersonalDetails','person_detail_id','id');
    }

}
