<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class House.
 *
 * @package namespace App\Entities;
 */
class House extends Model implements Transformable
{
    use TransformableTrait;

    protected $table='houses';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'person_detail_id',
        'tol_number',
        'house_number',
        'house_type',
        'house_area'
    ];

    public function peronalDetails(){
        return $this->belongsTo('App\Entities\PersonalDetails','person_detail_id','id');
    }

}
