<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class PersonalDetails.
 *
 * @package namespace App\Entities;
 */
class PersonalDetails extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'personal_details';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'person_image',
        'person_name',
        'age',
        'blood_group',
        'education',
        'occupation',
        'religion',
        'father_name',
        'mother_name',
        'grand_father_name',
        'spouse_name',
        'time_interval_at_foreign',
        'foreign_occupation'
    ];

    public function houses(){
        return $this->hasMany('App\Entities\House','person_detail_id','id');
    }
    public function memberDetails(){
        return $this->hasMany('App\Entities\MemberDetails','person_detail_id','id');
    }

}
