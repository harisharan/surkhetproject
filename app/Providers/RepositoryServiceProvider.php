<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\HouseRepository','App\Repositories\HouseRepositoryEloquent');
        $this->app->bind('App\Repositories\MemberDetailsRepository','App\Repositories\MemberDetailsRepositoryEloquent');
        $this->app->bind('App\Repositories\PersonalDetailsRepository','App\Repositories\PersonalDetailsRepositoryEloquent');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
