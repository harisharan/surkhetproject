<?php

namespace App\Http\Controllers;

use App\Repositories\MemberDetailsRepository;
use App\Validators\MemberDetailsValidator;
use Illuminate\Http\Request;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class MemberDetailsController extends ApiBaseController
{
    protected $memberDetailsRepository;
    protected $memberDetailsValidator;

    public function __construct(MemberDetailsRepository $memberDetailsRepository,
                                MemberDetailsValidator $memberDetailsValidator)
    {
        parent::__construct();
        $this->memberDetailsRepository = $memberDetailsRepository;
        $this->memberDetailsValidator = $memberDetailsValidator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            try {
                $this->memberDetailsValidator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
                $data = $this->memberDetailsRepository->create($request->all());
                return $this->respondWithMessage('Member detail created successfully', $data);
            } catch (ValidatorException $e) {
                return $this->respondWithError($e->getMessageBag());
            }
        } catch (\Exception $e) {
            return $this->respondWithError($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $allDetails = $this->memberDetailsRepository->find($id);
            return $this->respondWithMessage('Member detail retrieved successfully', $allDetails);
        } catch (\Exception $e) {
            return $this->respondWithError($e->getMessage());

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $data = $this->memberDetailsRepository->update($request->all(), $id);
            return $this->respondWithMessage('Member detail updated successfully', $data);
        } catch (\Exception $e) {
            return $this->respondWithError($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data = $this->memberDetailsRepository->find($id);
            $this->memberDetailsRepository->delete($id);
            return $this->respondWithMessage('Member detail deleted successfully', $data);
        } catch (\Exception $e) {
            return $this->respondWithError($e->getMessage());
        }
    }
}
