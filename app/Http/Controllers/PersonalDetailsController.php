<?php

namespace App\Http\Controllers;

use App\Repositories\HouseRepository;
use App\Repositories\PersonalDetailsRepository;
use App\Validators\HouseValidator;
use App\Validators\MemberDetailsValidator;
use App\Validators\PersonalDetailsValidatorValidator;
use Faker\Provider\File;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PersonalDetailsController extends ApiBaseController
{
    protected $personalDetailsRepository;
    protected $houseRepository;
    protected $personalDetailsValidator;
    protected $houseValidator;
    protected $memberDetailsValidator;
    protected $imageName;

    public function __construct(
        PersonalDetailsRepository $personalDetailsRepository,
        HouseRepository $houseRepository,
        PersonalDetailsValidatorValidator $personalDetailsValidator,
        HouseValidator $houseValidator,
        MemberDetailsValidator $memberDetailsValidator
    )
    {
        parent::__construct();
        $this->personalDetailsRepository = $personalDetailsRepository;
        $this->houseRepository = $houseRepository;
        $this->personalDetailsValidator = $personalDetailsValidator;
        $this->houseValidator = $houseValidator;
        $this->memberDetailsValidator = $memberDetailsValidator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $house_number = $request->house_number;
            $member_name = $request->member_name;
            $allDetails = $this->personalDetailsRepository
                ->with(['houses', 'memberDetails'])
                ->whereHas('houses', function ($query) use ($house_number) {
                    $query->where('house_number', 'like', '%' . $house_number . '%');
                })
                ->whereHas('memberDetails', function ($query) use ($member_name) {
                    $query->where('member_name', 'like', '%' . $member_name . '%');
                })
                ->orderBy('created_At', $request->sort ? $request->sort : 'DESC')
                ->findWhere([
                    ['blood_group', 'like', '%' . $request->blood_group . '%'],
                    ['occupation', 'like', '%' . $request->occupation . '%'],
                    ['age', 'like', '%' . $request->age . '%'],
                    ['person_name', 'like', '%' . $request->person_name . '%']
                ]);
            $currentPage = Paginator::resolveCurrentPage() - 1;
            $perPage = $request->limit ? $request->limit : 50;
            $currentPageSearchResults = $allDetails->slice($currentPage * $perPage, $perPage)->all();
            $allDetails = new LengthAwarePaginator(array_values($currentPageSearchResults), count($allDetails), $perPage);
            if ($allDetails->first() == null) {
                return $this->respondWithError("Personal detail Not Found");
            }
            return $this->respondWithMessage("Personal detials retrived successfully", $allDetails->toArray());
        } catch (\Exception $e) {
            $this->respondWithError($e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        DB::beginTransaction();
        try {
            try {
                if ($request->isMobile) {
                    $request->merge([
                        'houses' => 'to avoid validation',
                        'members' => 'to avoid validation'
                    ]);
                }
                $data = $request->all();
                $this->personalDetailsValidator->with($data)->passesOrFail(validatorinterface::RULE_CREATE);
                if ($request->hasFile('person_image')) {
                    $data = $request->except('person_image');
                    $data['person_image'] = $this->uploadImage($request->person_image);
                }
                $personData = $this->personalDetailsRepository->create($data);
                if ($request->isMobile) {
                    DB::commit();
                    return $this->respondWithMessage('Personal Detail created sucessfully', $personData->id);
                }
//                $house_array = explode('},', $request->houses);
                $house_array = json_decode($request->houses, TRUE);
//                echo count($house_array);
//                var_dump($house_array);
//                exit;
                foreach ($house_array as $houseData) {
                    $houseData['person_detail_id'] = 0;

                      $allhouse = $this->houseRepository->all();

                      if(count($allhouse)>1){
                        $house = $this->houseRepository->orderBy('created_at', 'DESC')->firstOrCreate();
                    $auto_house_number = (int)$house['id'];
                    $houseData['house_number'] = $houseData['tol_number'] . "_" . ($auto_house_number + 1);
                    }else{
                        $houseData['house_number'] = $houseData['tol_number'] . "_" . 1;
                    }

                    $this->houseValidator->with($houseData)->passesOrFail(validatorinterface::RULE_CREATE);
                    $this->personalDetailsRepository
                        ->with(['houses'])
                        ->find($personData->id)
                        ->houses()
                        ->create($houseData);
                }
                $member_array = json_decode($request->members, TRUE);

                foreach ($member_array as $memberData) {
                    $memberData['person_detail_id'] = 0;
                    $this->memberDetailsValidator->with($memberData)->passesOrFail(validatorinterface::RULE_CREATE);
                    $this->personalDetailsRepository
                        ->with(['memberDetails'])
                        ->find($personData->id)
                        ->memberDetails()
                        ->create($memberData);
                }

                DB::commit();
                return $this->respondWithMessage('Personal Detail with houses and members created sucessfully', $data);

            } catch (ValidatorException $e) {
                return $this->respondWithError($e->getMessageBag());
            }
        } catch (\Exception $e) {
            DB::rollback();
            $this->unlinkImage($this->imageName);
            return $this->respondWithError($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $allDetails = $this->personalDetailsRepository
                ->with(['houses', 'memberDetails'])
                ->find($id);
            return $this->respondWithMessage('Personal detail retrieved successfully', $allDetails);
        } catch (\Exception $e) {
            return $this->respondWithError($e->getMessage());

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
//        try {
//            $allDetails = $this->personalDetailsRepository
//                ->with(['houses', 'memberDetails'])
//                ->find($id);
//
//            if ($allDetails->first() == null) {
//                return $this->respondWithError("Personal detail Not Found");
//            }
//            return $this->respondWithMessage("Personal detials retrived successfully", $allDetails->toArray());
//        } catch (\Exception $e) {
//            $this->respondWithError($e->getMessage());
//        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $data = $request->all();
            if ($request->hasFile('person_image')) {
                $dataPrevious = $this->personalDetailsRepository->find($id);
                $this->unlinkImage($dataPrevious->person_image);
                $data = $request->except('person_image');
                $data['person_image'] = $this->uploadImage($request->person_image);
            }
            $this->personalDetailsRepository->update($data, $id);
            return $this->respondWithMessage('Personal detail updated sucessfully', $data);
        } catch (\Exception $e) {
            return $this->respondWithError($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $data = $this->personalDetailsRepository->find($id);
            $this->personalDetailsRepository->delete($id);
            $this->unlinkImage($data->person_image);
            DB::commit();
            return $this->respondWithMessage('Personal detail deleted successfully', $data);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->respondWithError($e->getMessage());
        }
    }

    public function uploadImage($imageFile)
    {
        $this->imageName = time() . '.' . $imageFile->getClientOriginalExtension();
        $imageFile->move(public_path('images'), $this->imageName);
        return $this->imageName;
    }

    public function unlinkImage($image_name)
    {
        $image = public_path('images/' . $image_name);
        if (file_exists($image)) {
            unlink($image);
        }
    }
}
