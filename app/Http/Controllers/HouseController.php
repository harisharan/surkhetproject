<?php

namespace App\Http\Controllers;

use App\Repositories\HouseRepository;
use App\Validators\HouseValidator;
use Illuminate\Http\Request;
use mysql_xdevapi\Exception;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class HouseController extends ApiBaseController
{
    protected $houseRepository;
    protected $houseValidator;

    public function __construct(HouseRepository $houseRepository, HouseValidator $houseValidator)
    {
        parent::__construct();
        $this->houseRepository = $houseRepository;
        $this->houseValidator = $houseValidator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            try {
                $this->houseValidator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
                $data = $this->houseRepository->create($request->all());
                return $this->respondWithMessage('House detail created successfully', $data);
            } catch (ValidatorException $e) {
                return $this->respondWithError($e->getMessageBag());
            }
        } catch (\Exception $e) {
            return $this->respondWithError($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $allDetails = $this->houseRepository->find($id);
            return $this->respondWithMessage('House detail retrieved successfully', $allDetails);
        } catch (\Exception $e) {
            return $this->respondWithError($e->getMessage());

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $data = $this->houseRepository->update($request->all(), $id);
            return $this->respondWithMessage('House detail updated successfully', $data);
        } catch (\Exception $e) {
            return $this->respondWithError($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data = $this->houseRepository->find($id);
            $this->houseRepository->delete($id);
            return $this->respondWithMessage('House detail deleted successfully', $data);
        } catch (\Exception $e) {
            return $this->respondWithError($e->getMessage());
        }
    }

    public function checkUnique($house_number)
    {
        try {
            $data = $this->houseRepository->findWhere(['house_number' => $house_number]);
            if (count($data) > 0) {
                throw new \Exception('Duplicate entry');
            }
        } catch (\Exception $e) {
            return $this->respondWithError($e->getMessage(), 409);
        }
    }
}
