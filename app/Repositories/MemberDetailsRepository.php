<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MemberDetailsRepository.
 *
 * @package namespace App\Repositories;
 */
interface MemberDetailsRepository extends RepositoryInterface
{
    //
}
