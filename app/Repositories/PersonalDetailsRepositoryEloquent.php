<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\PersonalDetailsRepository;
use App\Entities\PersonalDetails;
use App\Validators\PersonalDetailsValidator;

/**
 * Class PersonalDetailsRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class PersonalDetailsRepositoryEloquent extends BaseRepository implements PersonalDetailsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PersonalDetails::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
