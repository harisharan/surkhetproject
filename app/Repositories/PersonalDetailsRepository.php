<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PersonalDetailsRepository.
 *
 * @package namespace App\Repositories;
 */
interface PersonalDetailsRepository extends RepositoryInterface
{
    //
}
