<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\MemberDetailsRepository;
use App\Entities\MemberDetails;
use App\Validators\MemberDetailsValidator;

/**
 * Class MemberDetailsRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class MemberDetailsRepositoryEloquent extends BaseRepository implements MemberDetailsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MemberDetails::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
