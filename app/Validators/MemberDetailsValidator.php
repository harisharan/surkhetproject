<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class MemberDetailsValidator.
 *
 * @package namespace App\Validators;
 */
class MemberDetailsValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'person_detail_id' => ['numeric','required'],
            'member_name' => ['required'],
            'member_age' => ['numeric', 'required'],
            'member_blood_group' => ['required'],
            'member_education' => ['required'],
            'member_occupation' => ['required'],
            'member_religion' => ['required'],
            'member_father_name' => ['required'],
            'member_mother_name' => ['required'],
            'member_grand_father_name' => ['required']
        ],
        ValidatorInterface::RULE_UPDATE => [],
    ];
}
