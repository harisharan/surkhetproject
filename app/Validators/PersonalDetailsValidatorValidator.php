<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class PersonalDetailsValidatorValidator.
 *
 * @package namespace App\Validators;
 */
class PersonalDetailsValidatorValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'person_image' => ['image','mimes:jpeg,png,jpg','max:2048'],
            'person_name'=>['required'],
            'age' => ['numeric','required'],
            'blood_group'=>['required'],
            'education'=>['required'],
            'occupation'=>['required'],
            'religion'=>['required'],
            'father_name'=>['required'],
            'mother_name'=>['required'],
            'grand_father_name'=>['required'],
            'houses'=>['required'],
            'members'=>['required']
        ],
        ValidatorInterface::RULE_UPDATE => [],
    ];
}
