<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class HouseValidator.
 *
 * @package namespace App\Validators;
 */
class HouseValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'person_detail_id' => ['numeric','required'],
            'tol_number' => ['numeric','required'],
            'house_number'=>['required','unique:houses'],
            'house_type'=>['numeric','required'],
            'house_area'=>['required']
        ],
        ValidatorInterface::RULE_UPDATE => [],
    ];
}
